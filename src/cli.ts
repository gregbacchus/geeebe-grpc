import * as Case from 'case';
import * as fs from 'fs';
import * as ast from 'ts-simple-ast';

const argv = require('yargs').argv;

const json = JSON.parse(fs.readFileSync(argv.proto).toString());
const base = json.nested;

const project = new ast.Project();

Object.keys(base).forEach((namespace) => {
  const children = base[namespace].nested;

  const fileName = `${namespace}.ts`;
  if (fs.existsSync(fileName)) {
    fs.unlinkSync(fileName);
  }
  const file = project.createSourceFile(fileName);
  const ns = file.addNamespace({ name: namespace, isExported: true });

  const clientFactorySpec = ns.addInterface({ name: 'ClientFactory', isExported: true });
  const serviceBuilderSpec = ns.addInterface({ name: 'ServerBuilder', isExported: true });

  Object.keys(children).forEach((key) => {
    const child = children[key];
    const interfaceSpec = ns.addInterface({ name: key, isExported: true });

    if (child.methods) {
      clientFactorySpec.addMethod({ name: `get${key}`, returnType: key });
      const addSpec = serviceBuilderSpec.addMethod({ name: `add${key}`, returnType: `Promise<ServerBuilder>` });
      addSpec.addParameter({ name: 'impl', type: key });
    }

    if (child.fields) {
      Object.keys(child.fields).forEach((fieldName) => {
        const field = child.fields[fieldName];
        interfaceSpec.addProperty({ name: fieldName, type: field.type, hasQuestionToken: true });
      });
    }
    if (child.methods) {
      Object.keys(child.methods).forEach((methodName) => {
        const method = child.methods[methodName];
        const methodSpec = interfaceSpec.addMethod({ name: Case.camel(methodName), returnType: `Promise<${method.responseType}>` });
        methodSpec.addParameter({ name: 'request', type: method.requestType });
      });
    }
  });

  file.saveSync();
});
