// ATTRIBUTION based on work from https://github.com/kondi/rxjs-grpc

import * as grpc from 'grpc';

import { lookupPackage } from './utils';

interface DynamicMethods { [name: string]: any; }

export interface GenericServerBuilder {
  start(address: string, credentials?: any): void;
  forceShutdown(): void;
}

export function serverBuilder<T>(protoPath: string, packageName: string): T & GenericServerBuilder {
  const server = new grpc.Server();

  const builder: DynamicMethods = {
    start(address: string, credentials?: any) {
      server.bind(address, credentials || grpc.ServerCredentials.createInsecure());
      server.start();
    },
    forceShutdown() {
      server.forceShutdown();
    },
  } as GenericServerBuilder;

  const pkg = lookupPackage(grpc.load(protoPath), packageName);
  for (const name of getServiceNames(pkg)) {
    builder[`add${name}`] = function f(rxImpl: DynamicMethods) {
      server.addService(pkg[name].service, createService(pkg[name], rxImpl));
      return this;
    };
  }

  return builder as any;
}

// tslint:disable-next-line:variable-name
function createService(Service: any, rxImpl: DynamicMethods) {
  const service: DynamicMethods = {};
  for (const name in Service.prototype) {
    if (typeof rxImpl[name] === 'function') {
      service[name] = createMethod(rxImpl, name, Service.prototype);
    }
  }
  return service;
}

function createMethod(rxImpl: DynamicMethods, name: string, serviceMethods: DynamicMethods) {
  return serviceMethods[name].responseStream
    ? createStreamingMethod(rxImpl, name)
    : createUnaryMethod(rxImpl, name);
}

function createUnaryMethod(rxImpl: DynamicMethods, name: string) {
  return (call: any, callback: any) => {
    try {
      const response: Promise<any> = rxImpl[name](call.request, call.metadata);
      response.then((data) => {
        callback(null, data);
      }).catch(callback);
    } catch (error) {
      callback(error);
    }
  };
}

function createStreamingMethod(_rxImpl: DynamicMethods, _name: string) {
  return async (call: any) => {
    try {
      // TODO
      // const response: Observable<any> = rxImpl[name](call.request, call.metadata);
      // await response.forEach((data) => call.write(data));
    } catch (error) {
      call.emit('error', error);
    }
    call.end();
  };
}

export type ClientFactoryConstructor<T> = new (address: string, credentials?: any, options?: any) => T;

export function clientFactory<T>(protoPath: string, packageName: string, address: string, credentials?: any, options?: any): T {
  class Constructor {
    public readonly theArgs: any[];

    constructor(address: string, credentials?: any, options?: any) {
      this.theArgs = [
        address,
        credentials || grpc.credentials.createInsecure(),
        options,
      ];
    }
  }

  const prototype: DynamicMethods = Constructor.prototype;
  const pkg = lookupPackage(grpc.load(protoPath), packageName);
  for (const name of getServiceNames(pkg)) {
    prototype[`get${name}`] = function f(this: Constructor) {
      return createServiceClient(pkg[name], this.theArgs);
    };
  }

  // return Constructor as any as ClientFactoryConstructor<T>;
  return (new Constructor(address, credentials, options)) as any;
}

function getServiceNames(pkg: any) {
  return Object.keys(pkg).filter((name) => pkg[name].service);
}

// tslint:disable-next-line:variable-name
function createServiceClient(GrpcClient: any, args: any[]) {
  const grpcClient = new GrpcClient(...args);
  const rxClient: DynamicMethods = {};
  for (const name of Object.keys(GrpcClient.prototype)) {
    rxClient[name] = createClientMethod(grpcClient, name);
  }
  return rxClient;
}

function createClientMethod(grpcClient: DynamicMethods, name: string) {
  return grpcClient[name].responseStream
    ? createStreamingClientMethod(grpcClient, name)
    : createUnaryClientMethod(grpcClient, name);
}

function createUnaryClientMethod(grpcClient: DynamicMethods, name: string) {
  return (...args: any[]) => {
    return new Promise((resolve, reject) => {
      grpcClient[name](...args, (error: any, data: any) => {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });
  };
}

function createStreamingClientMethod(grpcClient: DynamicMethods, name: string) {
  return (...args: any[]) => {
    // return new Observable((observer) => {
    //   const call = grpcClient[name](...args);
    //   call.on('data', (data: any) => observer.next(data));
    //   call.on('error', (error: any) => observer.error(error));
    //   call.on('end', () => observer.complete());
    // });
    return grpcClient[name](...args);
  };
}
