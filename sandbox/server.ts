import { join } from 'path';
import { serverBuilder } from '../src';
import { helloworld } from './helloworld';

// Pass the path of proto file and the name of namespace
const server = serverBuilder<helloworld.ServerBuilder>(join(__dirname, 'helloworld.proto'), 'helloworld');

// Add implementation
server.addGreeter({
  sayHello(request: helloworld.HelloRequest) {
    return Promise.resolve({
      message: 'GRPC hello ' + request.name,
    });
  },
});

// Start the server to listen on port 50051
server.start('0.0.0.0:50051');
