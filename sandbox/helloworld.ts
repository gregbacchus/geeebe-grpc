export namespace helloworld {
  export interface ClientFactory {
    getGreeter(): Greeter;
  }

  export interface ServerBuilder {
    addGreeter(impl: Greeter): Promise<ServerBuilder>;
  }

  export interface Greeter {
    sayHello(request: HelloRequest): Promise<HelloReply>;
  }

  export interface HelloRequest {
    name?: string;
  }

  export interface HelloReply {
    message?: string;
  }
}
