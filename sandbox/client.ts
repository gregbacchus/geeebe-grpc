import { join } from 'path';
import { clientFactory } from '../src';
import { helloworld } from './helloworld';

// Pass the path of proto file and the name of namespace
const services = clientFactory<helloworld.ClientFactory>(
  join(__dirname, 'helloworld.proto'),
  'helloworld',
  'localhost:50051',
);

// Create a client connecting to the server
// const services = factory('localhost:50051');

// Get a client for the Greeter service
const greeter = services.getGreeter();

// Call the service by passing a sample.HelloRequest
greeter.sayHello({ name: 'world' }).then((response) => {
  console.log(`Greeting: ${response.message}`);
}).catch(console.error);
